
#Pull image of maven
FROM maven:3-jdk-8

#Copy Project 
COPY . .

#Download dependencies:
RUN mvn install